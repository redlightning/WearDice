/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.weardice;

import java.util.LinkedList;

/**
 * Helper object for managing logged roll results
 *
 * @author Michael Isaacson
 */
public class ResultLogContent {
    public static final LinkedList<ResultLogItem> ITEMS = new LinkedList<>();

    /**
     * Add the item to the start of the list, that way most recent results are on top
     * @param item the dice roll result to add
     */
    public static void addItem(final ResultLogItem item) {
        ITEMS.push(item);
    }

    /**
     * A item representing a piece of content.
     */
    public static class ResultLogItem {
        public final long id;
        public final String diceRolled;
        public final int rollResult;

        ResultLogItem(final long id, final String diceRolled, final int rollResult) {
            this.id = id; //Not currently displayed, but could be nice for future use
            this.diceRolled = diceRolled;
            this.rollResult = rollResult;
        }
    }
}
