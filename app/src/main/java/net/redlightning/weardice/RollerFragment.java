/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.weardice;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shawnlin.numberpicker.NumberPicker;

import java.util.Random;

/**
 * Fragment to display the dice rolling interface
 *
 * @author Michael Isaacson
 */
public class RollerFragment extends Fragment {
    private static final String TAG = RollerFragment.class.getCanonicalName();
    public static final Integer ORANGE = Color.rgb(255, 140, 0);
    public static final String[] DICE_SIDES = new String[]{"2", "3", "4", "6", "8", "10", "12", "20", "100"};
    private NumberPicker mNumberOfSides;
    private NumberPicker mNumberOfDice;
    private TextView mResultView;
    private ShakeDetector mShakeDetector;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    public RollerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final @NonNull LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_roller, container, false);

        mResultView = layout.findViewById(R.id.roll_result);

        mNumberOfDice = layout.findViewById(R.id.die_count);
        mNumberOfDice.setMinValue(1);
        mNumberOfDice.setMaxValue(20);

        mNumberOfSides = layout.findViewById(R.id.die_sides);
        mNumberOfSides.setDisplayedValues(DICE_SIDES);
        mNumberOfSides.setMaxValue(DICE_SIDES.length - 1);

        final LinearLayout roller = layout.findViewById(R.id.roller);
        roller.setOnClickListener((view) -> roll());

        if(mSensorManager != null) {
            mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mShakeDetector = new ShakeDetector();
            mShakeDetector.setOnShakeListener((count) -> roll());
        }
        return layout;
    }

    @Override
    public void onAttach(@NonNull final Context context) {
        super.onAttach(context);
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if(mSensorManager != null) {
            mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        } else {
            Log.w(TAG, "Unable to connect to the sensor service");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mSensorManager.unregisterListener(mShakeDetector);
    }

    /**
     * Calculate the random result of XdY dice rolls.
     */
    public void roll() {
        final int selected = mNumberOfSides.getValue();
        final String sideName = DICE_SIDES[selected];
        final int sides = Integer.parseInt(sideName);
        final int numberOfDice = mNumberOfDice.getValue();
        Log.d(TAG, numberOfDice + " d " + selected + " (" + sides + ")");
        final Random random = new Random();
        int roll = 0;
        for (int i = 1; i <= numberOfDice; i++) {
            roll += random.nextInt(sides) + 1;
        }

        //Add the result to the roll log
        final ResultLogContent.ResultLogItem resultItem = new ResultLogContent.ResultLogItem(System.currentTimeMillis(), mNumberOfDice.getValue() + "d" + sideName, roll);
        ResultLogContent.addItem(resultItem);
        RollResultsFragment.update();

        //Display the result
        mResultView.setText(String.valueOf(roll));
        final ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), Color.WHITE, ORANGE);
        colorAnimation.addUpdateListener((animator) -> mResultView.setTextColor((Integer) animator.getAnimatedValue()));
        colorAnimation.setDuration(1000);
        colorAnimation.start();
    }
}
