/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.weardice;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Handle rolling polyhedral dice on Android Wear watches
 *
 * @author Michael Isaacson
 */
public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DicePagerAdapter adapter = new DicePagerAdapter(getSupportFragmentManager());
        final ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(adapter);
    }

    /**
     * Handles switching between the two fragments
     */
    public static class DicePagerAdapter extends FragmentPagerAdapter {
        DicePagerAdapter(final FragmentManager fm) {
            super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int i) {
            if(i==0) {
                return new RollerFragment();
            } else {
                return new RollResultsFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
