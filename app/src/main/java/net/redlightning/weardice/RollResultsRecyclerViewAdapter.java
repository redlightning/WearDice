/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.weardice;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.redlightning.weardice.ResultLogContent.ResultLogItem;

import java.util.LinkedList;

/**
 * Adapter to handle roll result bindings
 *
 * @author Michael Isaacson
 */
public class RollResultsRecyclerViewAdapter extends RecyclerView.Adapter<RollResultsRecyclerViewAdapter.ViewHolder> {
    private final LinkedList<ResultLogItem> mValues;

    RollResultsRecyclerViewAdapter() {
        mValues = ResultLogContent.ITEMS;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(final @NonNull ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_rollresults, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull ViewHolder holder, final int position) {
        holder.mDiceRolledView.setText(String.valueOf(mValues.get(position).diceRolled));
        holder.mRollResultView.setText(String.valueOf(mValues.get(position).rollResult));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * View holder to handle roll result display
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mDiceRolledView;
        private final TextView mRollResultView;

        ViewHolder(final View view) {
            super(view);
            mDiceRolledView = view.findViewById(R.id.dice_rolled);
            mRollResultView = view.findViewById(R.id.roll_total);
        }
    }
}
